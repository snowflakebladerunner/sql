use hackathon5a;

load data infile '/var/lib/mysql-files/user_details.csv'
into table user_details
fields terminated by ','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 rows;

load data infile '/var/lib/mysql-files/trades.csv'
into table trade
fields terminated by ','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 rows;
