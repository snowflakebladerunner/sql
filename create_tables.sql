drop database if exists hackathon5a;
create database hackathon5a;
use hackathon5a;

drop table if exists user_details;
drop table if exists trade;
drop table if exists portfolio;

create table user_details (
	user_id integer not null AUTO_INCREMENT,
    user_name varchar(50),
    pan varchar(30) unique not null,
    email varchar(30),
    phone varchar(30),
    cash double,
    equity_value double,
    primary key (user_id)
);

create table trade (
    user_id integer,
	trade_id integer not null,
    trade_time timestamp,
    ticker_symbol varchar(50),
    order_side int,
    order_type varchar(50),
    quantity double not null,
    price double,
    status_code int,
    primary key (trade_id),
    foreign key (user_id) references user_details(user_id)
);

create table portfolio (
    user_id int not null,
	ticker_symbol varchar(50),
    quantity double,
    avg_price double,
    foreign key (user_id) references user_details(user_id)
);


insert into user_details(user_name, pan, email, phone, cash, equity_value) values("John", "ANUDFE8765E", "john@gmail.com", "+91 9956897812", 4589, 1258.2);
insert into user_details(user_name, pan, email, phone, cash, equity_value) values("Brian", "BNUDFE8765E", "bbrian@gmail.com", "+91 8956897818", 14589, 10258.2);
insert into user_details(user_name, pan, email, phone, cash, equity_value) values("Samuel", "SNUDFE8765F", "samsam@gmail.com", "+91 7956897815", 42589, 12058.2);
insert into user_details(user_name, pan, email, phone, cash, equity_value) values("Rohan", "RNUDFE8765K", "rohan3@gmail.com", "+91 6956897813", 445589, 12580.2);
insert into user_details(user_name, pan, email, phone, cash, equity_value) values("Manish", "MNUDFE8765D", "manish23@gmail.com", "+91 9956897815", 412589, 125028.2);

insert into trade(trade_id, user_id, trade_time, ticker_symbol, order_side, order_type, quantity, price, status_code) values(1,2,"2018-01-02 01:03:04", "AAPL", 0, "Market", 22, 21324, 0);
insert into trade(trade_id, user_id, trade_time, ticker_symbol, order_side, order_type, quantity, price, status_code) values(2,2,"2018-02-02 01:03:04", "REL", 0, "Market", 22, 213, 0);
insert into trade(trade_id, user_id, trade_time, ticker_symbol, order_side, order_type, quantity, price, status_code) values(3,1,"2019-01-03 01:03:04", "INFO", 1, "Limit", 22, 54, 1);
insert into trade(trade_id, user_id, trade_time, ticker_symbol, order_side, order_type, quantity, price, status_code) values(4,3,"2020-01-02 01:03:04", "TCS", 0, "GTC", 22, 324, 2);
insert into trade(trade_id, user_id, trade_time, ticker_symbol, order_side, order_type, quantity, price, status_code) values(5,5,"2021-05-04 01:03:04", "AAPL", 1, "Market", 22, 224, 0);

insert into portfolio(user_id, ticker_symbol, quantity, avg_price) values(1, "AAPL", 21, 20153);
insert into portfolio(user_id, ticker_symbol, quantity, avg_price) values(1, "REL", 221, 153);
insert into portfolio(user_id, ticker_symbol, quantity, avg_price) values(2, "AAPL", 321, 2153);
insert into portfolio(user_id, ticker_symbol, quantity, avg_price) values(3, "AAPL", 210, 253);
insert into portfolio(user_id, ticker_symbol, quantity, avg_price) values(1, "INFO", 1, 253);