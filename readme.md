# Instructions to create new database.

## Run create_tables.sql

# Deprecated

## Run create_tables.sql
This can be completed by using mysql workbench.

## Copy csv's to sql directory
The sql directory is C:\ProgramData\MySQL\MySQL Server 8.0\Uploads. Copy the csv's to this directory.

## Run insert_data_csv.sql
This will insert all the entries in the csv into the database.
